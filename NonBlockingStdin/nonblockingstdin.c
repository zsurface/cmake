#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>


// how to build
// cmake -H. -Bbuild
// cmake --build build -- -j3

// Sun Dec  8 13:46:02 2019 
// REF: https://www.reddit.com/r/shell/comments/e7o46r/print_string_to_cursor_position_without_a_newline/  
// KEY: write output to shell, print string to shell without newline, no newline
int main(int argc, char **argv) {

    // for (int i = 1; i < argc; ++i) {
        // for (char *p = argv[i]; *p; ++p) {
	    // // write_to_shell  "a b c"  => quote your arguments to one string
            // ioctl(STDIN_FILENO, TIOCSTI, p);
        // }
    // }

    int count = 0;

    for(;;){ 
        int flags = fcntl(STDIN_FILENO, F_GETFL);
        fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
        char ch = fgetc(stdin);
        printf("c=[%c]\n", ch);
        if ( ch == 'q' ){
            printf("count=%c\n", ch);
            break;
        }
        usleep(1000*1000);
        printf("count=%d\n", count);
        count++;
    }


}

