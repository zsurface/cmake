#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>



// how to build
// cmake -H. -Bbuild
// cmake --build build -- -j3

// Sun Dec  8 13:46:02 2019 
// REF: https://www.reddit.com/r/shell/comments/e7o46r/print_string_to_cursor_position_without_a_newline/  
// KEY: write output to shell, print string to shell without newline, no newline
int main(int argc, char **argv) {
    for (int i = 1; i < argc; ++i) {
        for (char *p = argv[i]; *p; ++p) {
	    // write_to_shell  "a b c"  => quote your arguments to one string
            ioctl(STDIN_FILENO, TIOCSTI, p);
        }
    }
}
