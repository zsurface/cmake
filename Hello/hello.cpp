// test.cpp

#include <iostream>

using namespace std;

// abacd => abcd

// There is no problem *str is not stored in stack.
// https://stackoverflow.com/questions/2589949/string-literals-where-do-they-go
char* getStr(){
  char *str = "this is static string";
  return str;
}


void removeDuplicate(char* oldPtr, char* newPtr){
  char ascii[256];
  // initialize array to zero
  for(int i=0; i<256; i++)
    ascii[i] = -1;

  int k = 0;
  for(int i=0; i<strlen(oldPtr); i++){
    int inx = oldPtr[i];
    if(ascii[inx] == -1){
      newPtr[k] = inx;
       ascii[inx] = inx;
       k++;
    }
  }
  newPtr[k] = '\0';
}

void ascii(){
  for(int i=0; i<=255; i++){
    printf("[%c][%d]", i, i);
  }
}

int main(void) {

     cout << "Hello World" << endl;
     std::cout<<"dig"<<endl;

     char* chPtr = "String abc";
     cout<<"chPtr="<<chPtr<<endl;
     cout<<"chPtr pointer to addr="<<(void*)chPtr<<endl;

     char arr[] = "My String";
     cout<<"Constant poiter="<<arr<<endl;

     char const* pt = "Hello World";
     cout<<"pointer to const [*pt]="<<*pt<<endl;
     cout<<"pointer to const [pt] ="<<pt<<endl;

     cout<<"static string="<<getStr()<<endl;

     ascii();


     char *myarr = "ababaabbccc";
     char* ptr = (char*)malloc((strlen(arr) + 1)*sizeof(char));
     if(ptr != NULL){
       removeDuplicate(myarr, ptr);
       cout<<"ptr="<<ptr<<endl;
     }
     free(ptr);

     cout<<"done!"<<endl;
     cout<<"my ptr="<<ptr<<endl;
     return(0);
}
