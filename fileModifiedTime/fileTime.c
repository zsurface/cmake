#define _POSIX_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#undef _POSIX_SOURCE
#include <stdio.h>
#include <time.h>

int main(int argc, char** argv) {
  struct stat info;

  if (argc > 1){
      if (stat(argv[1], &info) != 0){
        perror("stat() error");
      } else {
        printf("%ld",    info.st_mtime);
      }
  }else{
        printf("fileTime  /tmp/file.x  => latest modification time [/Users/cat/myfile/bitbucket/cmake/fileModifiedTime/fileTime.c] \n"); 
  }
}
