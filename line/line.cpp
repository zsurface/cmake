
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include "AronLib.h"
#include <boost/filesystem.hpp>
 
using namespace std;
using namespace AronLambda;


/**
 * \brief pass a lambde expression and replace variable with a value
 *
 * @expr = "x -> cp x /tmp"
 * 
 * @value = "/dog/file.x" 
 * 
 * @return = "cp /dog/file.x /tmp"
 *
 * TODO: use cmake
 * TODO: bug: If the argument contains spaces, then the return string is broken.
 * @expr = "x -> cp x /tmp" 
 * @value = "/dog/a b c.x"   => value contains space.
 * @return = "cp /dog/a /tmp" => error
 *
 * KEY: cpp stdin input
 */
string createExpOne(string expr, string value){
  bool DEBUG = false;
    // expr = "x -> cp x /tmp"
    // pair.first[0] => "x"
    // pair.second => {"cp", "x", "/tmp"}
    // pair ({"x"}, {"cp", "x", "/tmp"})
    std::pair<vector<string>, vector<string>> pair = parseLambda(expr);
    vector<string> vec;
    if(pair.first.size() > 0){
      string noSpValue = "\"" + value + "\"";
      vec = replaceVec(pair.first[0], noSpValue, pair.second);
    }
    else{
        vec = pair.second;
    }

    string ncmd = join(vec);
    // cout<<"ncmd="<<ncmd<<endl;
    return ncmd;
}

int main(int argc, char* argv[]) {
  // accept pipe input 
    if(argc == 2){
      string cmd = c2s(argv[1]); 
      std::pair<vector<string>, vector<string>> pair = parseLambda(c2s(argv[1]));
      string input;
      while(getline(cin, input)){
	// cout<<"input="<<input<<endl;
	string ncmd = createExpOne(c2s(argv[1]), input);
	std::system(s2c(ncmd)); // execute the UNIX command: (x -> cp x /tmp) => (file -> cp file /tmp) => (cp file /tmp) 
	  }
    }else{
      cout<<"Read line by line from stdin or pipe"<<endl;
      cout<<"cmd 'x -> cp x /tmp'"<<endl;
    }
}
