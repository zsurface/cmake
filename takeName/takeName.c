// #include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "AronCLibNew.h"
// #include "AronLib.h"
// #include <boost/filesystem.hpp>
 
// using namespace std;
// using namespace AronLambda;
//
// NOTE: DOES NOT depend on C++ or boost library

int main(int argc, char* argv[]) {
    if(argc == 2){
        int bufLen = strlen(argv[1]);
        char c = '/';
        int retRow = 0;
        char** ppt = splitPath(argv[1], bufLen, &retRow, c);
        printf(ppt[retRow-1]);
        free_splitPath(ppt, retRow);
    }else{
        printf("Need One argument ONLY\n");
    }
}
