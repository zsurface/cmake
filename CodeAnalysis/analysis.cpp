#include "AronLib.h"
#include <stack> 

/**
 * NOTE: The code have move to /Users/cat/myfile/bitbucket/xcode/CodeAnalysis/
 * 
 * Do not use the code any more.
 */
using namespace std;
// using namespace SpacePrint;

class Bracket{
    public:
    static const char PARENTTHESE_L = '(';
    static const char PARENTTHESE_R = ')';
    static const char CURLY_L       = '{';
    static const char CURLY_R       = '}';
    static const char ANGLE_L       = '<';
    static const char ANGLE_R       = '>';
    static const char SQUARE_L      = '[';
    static const char SQUARE_R      = ']';
};

class Item{
public:
    int lineNum;
    int inx;
    char c;
    public:
    Item(){}
    Item(int lineNum, int inx, char ch){
        this -> lineNum = lineNum;
        this -> inx = inx;
        this -> c = ch;
    }
    string toString(){
        string s = "";
        s += "lineNum=" + toStr(lineNum) + " " + " inx=" + toStr(inx) + " c=" + toStr(c);
        return s;
    }
    void print(){
        printf("%s\n", toString().c_str());
    }
};

bool balanceBracket(string s){
    return true;
}

void printStack(stack<Item> s){
    while(!s.empty()){
        pp(s.top().toString());
        s.pop();
    }
}


bool isBalanced(int lineNum, string line, Item& itemError){
    bool ret = true;
    bool done = false;
    std::stack<Item> st;
    // Iterate each line
    for(int i=0; i<line.size() && !done; i++){
        if(line[i] == Bracket::PARENTTHESE_L || line[i] == Bracket::ANGLE_L){
            Item item(lineNum, i, line[i]);
            st.push(item);
        }else if(line[i] == Bracket::PARENTTHESE_R){
            if(!st.empty()){
                Item item = st.top();
                st.pop();
                if(item.c != Bracket::PARENTTHESE_L){
                    itemError = item;
                    ret = false;
                    done = true; 
                }
            }else{
                Item item(lineNum, i, line[i]);
                itemError = item;
                ret = false;
                done = true;
            }
        }else if(line[i] == Bracket::ANGLE_R){
            if(!st.empty()){
                Item item = st.top();
                st.pop();
                if(item.c != Bracket::ANGLE_L){
                    itemError = item;
                    ret = false;
                    done = true;
                }
            }
        }

    }
    if(!done && !st.empty()){
        itemError = st.top();
        ret = false;
    }
    return ret;
}


void test0(){
    using namespace SpaceTest;
    {
        fw("test 1"); 
        int lineNum = 0;
        Item item;
        string line = "";
        bool ret = isBalanced(lineNum, line, item);
        t(ret, true);
    }
    {
        fw("test 2");
        int lineNum = 0;
        Item item;
        string line = "(";
        bool ret = isBalanced(lineNum, line, item);
        t(ret, false);
        t(item.lineNum, 0);
        t(item.inx, 0);
        t(item.c, '(');
    }
    {
        char arr[][10] = {"hi", "bb"};
        cout<<arr[0]<<endl;
        // arr[0] = "kkkk";
        cout<<arr[0]<<endl;
        
        char* arr1[10] = {"hi", "bb"};
        arr1[0] = "kkk";
        cout<<arr1[0]<<endl;
        
//        char (*a3[])[] = { &"blah", &"hmm" };
//        a3[0] = &"hello";
//        cout<<"Never knew that in C => *a3[0]="<< *a3[0]<<endl;
    }
}

int main(int argc, char** argv) {
  test0();
  // pp(argc);
  // if(argc == 2){
    // pp(argv[0]); 
    // pp(argv[1]); 
    // int lineNum = 0;
    // Item item;
    // bool ret = isBalanced(lineNum, toStr(argv[1]), item);
    // if(!ret){
        // pp("error");
        // pp("lineNum=" + toStr(item.lineNum));
        // pp("inx=" + toStr(item.inx));
        // pp("c=" + toStr(item.c));
    // }
  // }else{
      // pp("One argument only");
  // }
  
  return 0;
}
